import random
import threading
import time

random.seed(0)

# Generate a random float as an output, and wait that many seconds to simulate work being done
def var1(v1):
  result = random.random() / random.random()

  v1.append(result)
  time.sleep(result)


# Get user input at the same time a random number is being generated and the program is waiting
def var2(v2):
  user_input = input("Enter anything: ")
  v2.append(user_input)


if __name__ == "__main__":
  v1 = []
  v2 = []

  # Open the file to output the result to
  # This will overwrite any data currently stored in the file
  with open("output.txt", "w") as f:
    f.write("Random float:\t\tUser Input:\n")	# Formatting

    # Output n lines to the file, this will also prompt the user n times
    for i in range(5):
      # Declare the thread variables
      t1 = threading.Thread(target=var1, args=(v1,))
      t2 = threading.Thread(target=var2, args=(v2,))

      # Start the threads
      t1.start()
      t2.start()
      # Wait for both threads to finish before continuing in main
      t1.join()
      t2.join()

      # Output the random float and user input to the file
      f.write(str(v1[i]) + "\t" + v2[i] + "\n")
