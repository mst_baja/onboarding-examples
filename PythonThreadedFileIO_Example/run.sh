# Clear the screen
clear

# Run the program
python3 ThreadedFileIO.py

# Output the result
printf "\n"		# Print a newline in between the user's input and the file output
cat output.txt
