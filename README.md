<h1>Onboarding Examples</h1>

<pre>
In this repository are three examples (2 in C++ and 1 in Python) that show examples of file IO and multithreading.
While we will not use multithreading in the sense that it is depicted here, those examples are meant more to
simulate the dual CPU nature of the Arduino Portenta H7. The code can be compiled and ran by typing "./run.sh".
This Bash script will compile (C++ only), run the code, and display the output if it is saved to a text file.

This team will code with a Linux OS, or Linux VM if you are running Mac/Windows
(More about how to set up and use a VM below).
</pre>

<h1>How to Setup a Linux VM</h1>

<pre>
Here is a tutorial on how to setup and use a Linux VM if you have never done it before. If you are a Computer Science
major you will have to do this eventually.

1. Install <a href="https://www.virtualbox.org/wiki/Downloads">Oracle VirtualBox</a>
2. Install your preferred Linux distro
  <a href="https://www.debian.org/download">&bull; Debian</a>
  <a href="https://getfedora.org/en/workstation/download/">&bull; Fedora</a>
  <a href=https://ubuntu.com/download/desktop>&bull; Ubuntu (Recommended)</a>
3. Setting up your VM
  a. Open up VirtualBox and select the blue "New" icon
  b. Give the installation a name, create a folder to store the VM for "Machine Folder", and choose your Linux distro
  c. Set the system memory for the OS (Choose about half the amount of RAM your system currently has)
  d. Select "Create a virtual hard disk now" (Should be default option)
  e. Select "VDI" for the hard disk type (Should be default option)
  f. Select "Fixed size", and choose around 40GB if you have space (Should be enough to last throughout college)
  g. Open the VM and when prompted select the .iso file you downloaded earlier
  h. Follow the setup as prompted on Linux
  i. Install GuestAdditions (Allows you to fullscreen VirtualBox)
4. Install the <a href="https://www.arduino.cc/en/software">Arduino IDE</a>
  a. Install and extract the folder
  b. Open the folder location in terminal and type: "sudo ./install.sh"

<h2>Useful Linux Commands</h2>

cd [Directory]
  &bull; "Change Directory", or change folder. Use "../" to go back and "~" to go back to the start
ls
  &bull; View contents of a directory
cat [File]
  &bull; Outputs contents of file to the console. Great for viewing files without having to open them
cp [File] [Destination]
  &bull; Copies specified file to specified location
mv [File] [Destination]
  &bull; Moves specified file to specified location. Useful for renaming files
mkdir [Name]
  &bull; Creates a folder
rmdir [Directory]
  &bull; Deletes a directory
rm [File]
  &bull; Deletes a file
sudo [Command]
  &bull; SuperUser DO; executes command as a super user
man [Command]
  &bull; Gives a tutorial about any specified command (i.e., man rmdir)
nano [File]
  &bull; Basic text editor that allows you to create files, view files, write code, etc. Just remember to put the file extension
    at the end (.txt = text file, .py = Python code, .cpp = C++ code, etc.)
</pre>
