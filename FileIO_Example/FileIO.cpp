#include <iostream>
#include <fstream>

using namespace std;

int main()
{
  ofstream fout;
  int amnt_times;

  // Take input from the user and make sure that it is an integer
  cout << "Enter number of lines to output to file: ";
  cin >> amnt_times;

  // If the user enetered a character/string, prompt the user again
  while (cin.fail())
  {
    cout << "Please enter an integer: ";
    cin.clear();
    cin.ignore(256, '\n');
    cin >> amnt_times;
  }

  // Open a file and output the number of lines the user entered
  fout.open("output.txt");
  for (int i = 0; i < amnt_times; ++i)
  {
    fout << i << endl;
  }
  fout.close();

  return 0;
}
