# Clear screen
clear

# Compile program
g++ -g -Wall --pedantic-errors -O2 FileIO.cpp -o a.out

# Run program
./a.out

# Output the results from the text file
cat output.txt
