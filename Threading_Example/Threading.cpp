#include <iostream>
#include <thread>
#include <math.h>	// log10()

using namespace std;

/*
Simultaneously output a processing message while amnt is being incremented in main()

The number of "."s after "Waiting" corresponds to a log base 10 function of the number the user input
*/
void waiting(int amnt)
{
  cout << "Waiting";
  for (int i = 0; i < amnt; ++i)
  {
    cout << ".";
  }
}

int main()
{
  int amnt_times;
  int amnt = 0;

  // Take input from the user and make sure that it is an integer
  cout << "Enter number of times to loop: ";
  cin >> amnt_times;

  // If the user enetered a character/string, prompt the user again
  while (cin.fail())
  {
    cout << "Please enter an integer: ";
    cin.clear();
    cin.ignore(256, '\n');
    cin >> amnt_times;
  }

  // Start thread to output a processing message in parallel with doing work in main()
  thread t(waiting, log10(amnt_times));
  // Increment a variable the number of times the user specified to loop
  for (int i = 0; i < amnt_times; ++i)
  {
    ++amnt;
  }
  // Once waiting() and the for loop above have ended, rejoin the threads (2 -> 1)
  t.join();

  // Output end result
  cout << "\nLooped " << amnt << " times" << endl;

  return 0;
}
