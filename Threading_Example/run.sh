# Clear screen
clear

# Compile program
g++ -g -Wall --pedantic-errors -O2 Threading.cpp -o a.out -pthread

# Run program
./a.out
